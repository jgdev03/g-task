import jwt from 'jsonwebtoken';
import { config } from '../config';
import { jsonResponse } from '../helpers/helper';
//models
import User from '../models/User'

export function authenticateToken(req, res, next) {
    //verificamos que exista la cebecera utenticacion
    if(!req.headers.authorization){
        return jsonResponse(res, 401, 'No esta autenticado', null)
    }
    //obtenemos el token de la cabecera
    const token = req.headers.authorization
    if (token == null) {
    }
    //decodificamos el token
    jwt.verify(token, config.secret, (err, data) => {
        if (err) {
            return jsonResponse(res, 403, 'Autenticacion invalida', null)
        }
        req.user = data.user
        next() // continuamos
    })
}