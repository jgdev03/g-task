import { DataTypes  } from 'sequelize';
import { sequelize } from '../database/conexion'

const Task = sequelize.define('Task', {
    id: {  
        type: DataTypes.INTEGER,
        allowNull:false,
        primaryKey:true,
        autoIncrement:true
    },
    nombre: {  
        type: DataTypes.STRING(400),
        allowNull:false 
    },
    completa: {  
        type: DataTypes.INTEGER,
        defaultValue:0,
        allowNull:false 
    },
    list_id: {  
        type: DataTypes.INTEGER,
        allowNull:false 
    },
},
{
    tableName:'task'
});

export default Task;