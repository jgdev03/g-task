import { DataTypes  } from 'sequelize';
import { sequelize } from '../database/conexion'
//models
import Task from './Task';

const List = sequelize.define('list', {
    id: {  
        type: DataTypes.INTEGER,
        allowNull:false,
        primaryKey:true,
        autoIncrement:true
    },
    nombre: {  
        type: DataTypes.STRING(400),
        allowNull:false 
    },
    user_id: {  
        type: DataTypes.INTEGER,
        allowNull:false
    }
},{
    tableName:'list'
});

List.hasMany(Task, {foreignKey : 'list_id'})
Task.belongsTo(List, {foreignKey : 'list_id'})

export default List;