import { DataTypes } from 'sequelize';
import { sequelize } from '../database/conexion'

import List from './List';

const User = sequelize.define('user', {
    id: {  
        type: DataTypes.INTEGER,
        allowNull:false,
        primaryKey:true,
        autoIncrement:true
    },
    nombre: {  
        type: DataTypes.STRING(400),
        allowNull:false 
    },
    email: {  
        type: DataTypes.STRING(400),
        allowNull:false 
    },
    password: {  
        type: DataTypes.STRING(400),
        allowNull:false 
    },
    celular: {  
        type: DataTypes.STRING(15),
        allowNull:false 
    }
},{
    tableName:'user'
});

User.hasMany(List, {foreignKey : 'user_id'})
List.belongsTo(User, {foreignKey : 'user_id'})

export default User;