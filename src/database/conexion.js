import Sequelize from 'sequelize';

export const sequelize = new Sequelize(
    'gtask', 
    'dev03',
    'smktest20',
    {
        host:'localhost',
        dialect: 'mysql',
        pool: {
            max:10,
            min:0,
            require:30000,
            idle:10000
        },
        logging: false
    }
)