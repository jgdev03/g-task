import express from 'express';
import morgan from 'morgan';
//cors
import cors from 'cors'
//db
import { sequelize } from './database/conexion';
//routes import
import user from './routes/user';
import list from './routes/list';
import task from './routes/task';
//instanciamos express
const app = express();
//middleware
app.use(cors());
app.use(morgan('combined'));
app.use(express.urlencoded({ extended: true }));
app.use(express.json())
//routes api
app.use('/api/user', user);
app.use('/api/list', list);
app.use('/api/task', task);
//nos authenticamos con la BD
sequelize.authenticate().then( r => {
    console.log('Database connect...');
    //corremos el server
    app.listen(3000, () => {  console.log("server run ...") });
}).catch( error => {
    console.log('Database connection error...', error );
})
