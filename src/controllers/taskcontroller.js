//models
import List from '../models/List';
import User from '../models/User';
import Task from '../models/Task';
//Helpers
import { jsonResponse, parseInt } from '../helpers/helper'
//librarys
import moment from 'moment';

//listar
export async function getAll(req, res){
    const { user } = req;
    const { listId } = req.params
    //buscamos el usuarion
    const userModel = await User.findOne({ where: { email: user } });
    //validamos si se encontro el usuario
    if (userModel == null){
        return jsonResponse(res, -1, 'Usuario no encontrado', null);
    }
    //validamos el parametro id
    if (listId == null || listId == undefined){
        return jsonResponse(res, -2, 'Parametro invalido', null);
    }
    //parseamos el parametro a entero
    const id = parseInt(listId)
    //verificamos el tipo de dato
    if (id == -1){
        return jsonResponse(res, -3, 'Parametro invalido', null);
    }
    //intenamos obtener la lista
    const list = await List.findOne({ where: { user_id: userModel.id, id: id } });
    if (list == null){
        return jsonResponse(res, -4, 'Lista no encontrada', null);
    }
    //buscamos las tareas
    const tasks = await Task.findAll({ 
        attributes: [ 'id', 'nombre', 'createdAt', 'completa' ], 
        where: { list_id: list.id } 
    })
    return jsonResponse(res, 200, 'Exito', tasks);
}
//crear -update
export async function createOrUpdate(req, res){
    const { user } = req;
    //buscamos el usuarion
    const userModel = await User.findOne({ where: { email: user } });
    //validamos si se encontro el usuario
    if (userModel == null){
        return jsonResponse(res, -1, 'Usuario no encontrado', null);
    }
    const { nombre, id, actionId, listId, completa } = req.body;
    // validamos que exista el actionId
    if (actionId == null || actionId == undefined){
        return jsonResponse(res, -2, 'Parametro invalido', null);
    }
    //validamos el tipo de dato
    if (typeof actionId !== 'number'){
        return jsonResponse(res, -3, 'Parametro invalido', null);
    }
    // validamos que exista listId
    if (listId == null || listId == undefined){
        return jsonResponse(res, -4, 'Parametro invalido', null);
    }
    //validamos el tipo de dato
    if (typeof listId !== 'number'){
        return jsonResponse(res, -5, 'Parametro invalido', null);
    }
    //verificamos si existe la lista
    const listModel = await List.findOne({ where: { id: listId, user_id: userModel.id } })
    if (listModel == null){
        return jsonResponse(res, -6, 'Lista no encontrada', null);
    }
    //verificamos la accion 1- crear, 2 - actualizar
    if (actionId == 1){
        //validamos el nombre
        if (nombre == '' || nombre == null || nombre == undefined){
            return jsonResponse(res, -7, 'Parametro invalido', null);
        }
        //validamos el tipo de dato
        if (typeof nombre !== 'string'){
            return jsonResponse(res, -8, 'Parametro invalido', null);
        }
        //validamos la longitud
        if (nombre.length > 390){
            return jsonResponse(res, -9, 'Logitud invalido', null);
        }
        //creamos la lista
        const newtask = await Task.create({ nombre: nombre, list_id: listModel.id, completa:0 });
        try{
            return newtask ? jsonResponse(res, 200, 'Exito', null) : jsonResponse(res, -10, 'No se registro tarea', null);
        }
        catch(e){
            return jsonResponse(res, -11, 'Ocurrio un error al crear tarea', null);
        }
    }
    else if (actionId == 2){
        //verificamos el id
        if(id == null || id == undefined){
            return jsonResponse(res, -12, 'Parametro invalido', null);
        }
        //validamos el tipo de dato
        if (typeof id !== 'number'){
            return jsonResponse(res, -13, 'Parametro invalido', null);
        }
        //verificamos que venga el parametro completo
        if (completa == null || completa == undefined){
            return jsonResponse(res, -14, 'Parametro invalido', null);
        }
        //validamos el tipo de data
        if (typeof completa !== 'number'){
            return jsonResponse(res, -15, 'Parametro invalido', null);
        }
        //buscamos la lista
        const taskModel = await Task.findOne({ where: { id: id, list_id: listModel.id } })
        if (taskModel == null){
            return jsonResponse(res, -16, 'Tarea no encontrada', null);
        }
        //actalizamos
        taskModel.completa = completa
        taskModel.updateAt = moment().toDate()
        taskModel.save().then( r => {
            jsonResponse(res, 200, 'Exito', null)
        }).catch( e => {
            jsonResponse(res, -17, 'No se actualizo tarea', null);
        })
    }
    else{
        return jsonResponse(res, -18, 'Accion invalida', null)
    }
}
//delete
export async function deleteTask(req, res){
    const { user } = req;
    //buscamos el usuarion
    const userModel = await User.findOne({ where: { email: user } });
    //validamos si se encontro el usuario
    if (userModel == null){
        return jsonResponse(res, -1, 'Usuario no encontrado', null);
    }
    const { listId, id } = req.params;
    // validamos que exista el actionId
    if (id == null || id == undefined){
        return jsonResponse(res, -2, 'Parametro invalido', null);
    }
    const idInt = parseInt(id)
    //validamos el tipo de dato
    if (idInt == -1){
        return jsonResponse(res, -3, 'Parametro invalido', null);
    }
    // validamos que exista el actionId
    if (listId == null || listId == undefined){
        return jsonResponse(res, -4, 'Parametro invalido', null);
    }
    const listIdInt = parseInt(listId)
    //validamos el tipo de dato
    if (listIdInt == -1){
        return jsonResponse(res, -5, 'Parametro invalido', null);
    }
    //buscamos la lista
    const listModel = await List.findOne({ where: { id: listIdInt, user_id: userModel.id } })
    if (listModel == null){
        return jsonResponse(res, -6, 'Lista no encontrada', null);
    }
    //buscamos  la tarea
    const taskModel = await Task.findOne({ where: { id: idInt, list_id: listModel.id } })
    if (taskModel == null){
        return jsonResponse(res, -7, 'Tarea no encontrada', null);
    }
    //eliminamos
    taskModel.destroy().then( r => {
        jsonResponse(res, 200, 'Exito', null)
    }).catch( e => {
        console.log(e)
        jsonResponse(res, -8, 'No se elimino tarea', null);
    })
}