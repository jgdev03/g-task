//models
import List from '../models/List';
import User from '../models/User';
//Helpers
import { jsonResponse } from '../helpers/helper'
//librarys
import moment from 'moment'
import Task from '../models/Task';

//listar
export async function getAll(req, res){
    const { user } = req;
    //buscamos el usuarion
    const userModel = await User.findOne({ where: { email: user } });
    //validamos si se encontro el usuario
    if (userModel == null){
        return jsonResponse(res, -1, 'Usuario no encontrado', null);
    }
    //intenamos obtener la lista
    const lists = await List.findAll({ 
        attributes: [ 'id', 'nombre', 'createdAt' ], 
        where: { user_id: userModel.id } 
    });
    return jsonResponse(res, 200, 'Exito', lists);
}
//listar cantidad
export async function getCantidad(req, res){
    const { user } = req;
    //buscamos el usuarion
    const userModel = await User.findOne({ where: { email: user } });
    //validamos si se encontro el usuario
    if (userModel == null){
        return jsonResponse(res, -1, 'Usuario no encontrado', null);
    }
    //intenamos obtener la lista
    await List.count({ where: { user_id: userModel.id } }).then( c => {
        return jsonResponse(res, 200, 'Exito', c);
    })
}

//crear -update
export async function createOrUpdate(req, res){
    const { user } = req;
    //buscamos el usuarion
    const userModel = await User.findOne({ where: { email: user } });
    //validamos si se encontro el usuario
    if (userModel == null){
        return jsonResponse(res, -1, 'Usuario no encontrado', null);
    }
    const { nombre, id, actionId } = req.body;
    // validamos que exista el actionId
    if (actionId == null || actionId == undefined){
        return jsonResponse(res, -2, 'Parametro invalido', null);
    }
    //validamos el tipo de dato
    if (typeof actionId !== 'number'){
        return jsonResponse(res, -3, 'Parametro invalido', null);
    }
    //validamos el nombre
    if (nombre == '' || nombre == null || nombre == undefined){
        return jsonResponse(res, -4, 'Parametro invalido', null);
    }
    //validamos el tipo de dato
    if (typeof nombre !== 'string'){
        return jsonResponse(res, -5, 'Parametro invalido', null);
    }
    //validamos la longitud
    if (nombre.length > 390){
        return jsonResponse(res, -6, 'Logitud invalido', null);
    }
    //verificamos la accion 1- crear, 2 - actualizar
    if (actionId == 1){
        //creamos la lista
        const newlist = await List.create({ nombre: nombre, user_id: userModel.id });
        try{
            return newlist ? jsonResponse(res, 200, 'Exito', newlist.id) : jsonResponse(res, -7, 'No se registro lista', null);
        }
        catch(e){
            return jsonResponse(res, -8, 'Ocurrio un error al crear lista', null);
        }
    }
    else if (actionId == 2){
        //verificamos el id
        if(id == null || id == undefined){
            return jsonResponse(res, -9, 'Parametro invalido', null);
        }
        //validamos el tipo de dato
        if (typeof id !== 'number'){
            return jsonResponse(res, -10, 'Parametro invalido', null);
        }
        //buscamos la lista
        const listModel = await List.findOne({ where: { id: id, user_id: userModel.id } })
        if (listModel == null){
            return jsonResponse(res, -11, 'Lista no encontrada', null);
        }
        //actalizamos
        listModel.nombre = nombre
        listModel.updateAt = moment().toDate()
        listModel.save().then( r => {
            jsonResponse(res, 200, 'Exito', null)
        }).catch( e => {
            console.log(e)
            jsonResponse(res, -12, 'No se actualizo lista', null);
        })
    }
    else{
        return jsonResponse(res, -13, 'Accion invalida', null)
    }
}
//delete
export async function deleteList(req, res){
    const { user } = req;
    //buscamos el usuarion
    const userModel = await User.findOne({ where: { email: user } });
    //validamos si se encontro el usuario
    if (userModel == null){
        return jsonResponse(res, -1, 'Usuario no encontrado', null);
    }
    const { id } = req.params;
    // validamos que exista el actionId
    if (id == null || id == undefined){
        return jsonResponse(res, -2, 'Parametro invalido', null);
    }
    //parseamos el parametro a entero
    const idInt = parseInt(id)
    //verificamos el tipo de dato
    if (idInt == -1){
        return jsonResponse(res, -3, 'Parametro invalido', null);
    }
    //buscamos la lista
    const listModel = await List.findOne({ where: { id: idInt, user_id: userModel.id } })
    if (listModel == null){
        return jsonResponse(res, -4, 'Lista no encontrada', null);
    }
    //eliminamos todas las tareas de la lista 
    await Task.destroy({ where: { list_id: listModel.id }  })
    //eliminamos la tarea
    listModel.destroy().then( r => {
        jsonResponse(res, 200, 'Exito', null)
    }).catch( e => {
        console.log(e)
        jsonResponse(res, -5, 'No se elimino lista', null);
    })
}