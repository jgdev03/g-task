//librarys
import crypto from 'crypto'
//models
import  User  from '../models/User';
//Helpers
import { jsonResponse, createToken } from '../helpers/helper'

const regexEmail = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/;
const regexPassword = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&#.$($)$-$_])[A-Za-z\d$@$!%*?&#.$($)$-$_]{8,15}$/;

export async function signUp(req, res){
    const { nombre, email, celular, password } = req.body
    //validamos la data
    if(nombre == null || nombre == undefined || nombre == ''){
      return jsonResponse(res, -1, 'Parametro invalido', null )
    }
    if(email == null || email == undefined || email == ''){
      return jsonResponse(res, -2, 'Parametro invalido', null )
    }
    if(celular == null || celular == undefined || celular == ''){
      return jsonResponse(res, -3, 'Parametro invalido', null )
    }
    if(password == null || password == undefined || password == ''){
      return jsonResponse(res, -4, 'Parametro invalido', null )
    }
    // validamos que todos sean un string
    if( typeof nombre !== 'string' || typeof email !== 'string' ||  typeof celular !== 'string' || typeof password !== 'string'){
        return jsonResponse(res, -5, 'Valor(es) invalido(s)', null )
    }
    //validamos la longitud de cada string
    if (nombre.length > 390 ){
        return jsonResponse(res, -6, 'Longitud de nombre no es permitida', null )
    }
    if (email.length > 390 ){
        return jsonResponse(res, -7, 'Longitud de email no es permitida', null )
    }
    if (celular.length > 10 ){
        return jsonResponse(res, -8, 'Longitud de celular no es permitida', null )
    }
    //validamos si es un correo
    if (!regexEmail.test(email)){
        return jsonResponse(res, -9, 'Email invalido', null )
    }
    //validamos si es una contraseña robusta
    if (!regexPassword.test(password)){
        return jsonResponse(res, -10, 'Contraseña invalida, debe contener al menos una letra mayuscula, minuscula, numero y caracter especial. La longitud debe estar entre 8 a 15 caracteres', null )
    }
    //verificamos su no existe el email
    const existe = await User.findOne({ where: { email: email } });
    if (existe != null){
        return jsonResponse(res, -11, 'Email ya esta registrado', null )
    }
    const hashpassword = crypto.createHmac('sha256', password).digest('hex').toString()
    //creamos el usuario
    try{
        const newuser = await User.create({ 
            nombre: nombre, 
            email: email, 
            password: hashpassword, 
            celular: celular
        })

        return newuser ? jsonResponse(res, 200, 'Exito', null) : jsonResponse(res, -12, 'No se registro usuario', null)
    }
    catch(e){
        console.log("error to insert", e)
        return jsonResponse(res, -13, 'Ocurrio un error al registrar usuario, intente nuevamente', null)
    }
}

export async function login(req, res) {
    const { email, password } = req.body
    //validamos la data
    if(email == null || email == undefined || email == ''){
      return jsonResponse(res, -1, 'Parametro invalido', null )
    }
    if(password == null || password == undefined || password == ''){
      return jsonResponse(res, -2, 'Parametro invalido', null )
    }
    //hash password
    const hashpassword = crypto.createHmac('sha256', password).digest('hex').toString()
    //verificamos su no existe el email
    const user = await User.findOne({ where: { email: email } });
    if (user == null){
        return jsonResponse(res, -3, 'No encontrado', null )
    }
    //validamos las password
    if (user.password != hashpassword){
        return jsonResponse(res, -4, 'Contraseña invalida', null )
    }
    //obtenemos el token
    const jwt = createToken(user)
    return jsonResponse(res, 200, 'Exito', { token: jwt, nombre: user.nombre }) 
}