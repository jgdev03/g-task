import jwt from 'jsonwebtoken';
import moment from 'moment';
import { config } from '../config';

export function jsonResponse(res, code, message, data){
    const codeallows = [ 200, 400, 500, 401, 403, 404, 405 ]
    if (codeallows.includes(code)){
        return res.status(code).json({code:code, data:data, message: message})
    }
    else{
        return res.status(400).json({code:code, data:data, message: message})
    }
}

export function createToken(user){
    const sub = moment().unix().toString()
    const token = jwt.sign({ user: user.email } , config.secret, { expiresIn: '12h', algorithm:'HS256', jwtid: sub })
    return token
}

export function parseInt(valuestr=''){
    try{
        return Number(valuestr)
    }
    catch(e){
        return -1
    }
}