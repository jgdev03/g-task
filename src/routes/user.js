import express from 'express';
//import usercontroller
import { signUp, login } from './../controllers/usercontroller';

const router = express.Router();
//routes
router.post('/create', signUp);
router.post('/login', login);

export default router;