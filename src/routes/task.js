import express from 'express';
//import usercontroller
import { getAll, createOrUpdate, deleteTask } from './../controllers/taskcontroller';
//middleware
import { authenticateToken } from './../middlewares/auth'

const router = express.Router();
//middleware
router.use(authenticateToken)
//route
router.get('/:listId', getAll);
router.post('/createOrUpdate', createOrUpdate);
router.delete('/delete/:listId/:id', deleteTask);

export default router;