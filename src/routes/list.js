import express from 'express';
//import usercontroller
import { getAll, createOrUpdate, deleteList, getCantidad } from './../controllers/listcontroller';
//middleware
import { authenticateToken } from './../middlewares/auth'

const router = express.Router();
//middleware
router.use(authenticateToken)
//route
router.get('/', getAll);
router.get('/count', getCantidad);
router.post('/createOrUpdate', createOrUpdate);
router.delete('/delete/:id', deleteList);

export default router;